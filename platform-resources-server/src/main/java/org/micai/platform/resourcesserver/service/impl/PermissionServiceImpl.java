package org.micai.platform.resourcesserver.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.resourcesserver.bo.PermissionDelBo;
import org.micai.platform.resourcesserver.bo.PermissionFindBo;
import org.micai.platform.resourcesserver.bo.PermissionSaveBo;
import org.micai.platform.resourcesserver.bo.PermissionUpdateBo;
import org.micai.platform.resourcesserver.dto.UserAuthenticationDto;
import org.micai.platform.resourcesserver.entity.Permission;
import org.micai.platform.resourcesserver.mapper.PermissionMapper;
import org.micai.platform.resourcesserver.service.PermissionService;
import org.micai.platform.resourcesserver.utils.AuthenticationManger;
import org.micai.platform.resourcesserver.vo.PermissionListVo;
import org.micai.platform.resourcesserver.vo.RoleListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public Page<PermissionListVo> getPermissionList(PermissionFindBo bo) throws Exception {
        Page<Permission> page = new Page<>(bo.getPageNumber(), bo.getPageSize());
        Page<Permission> permissionPage = permissionMapper.selectPage(page, new QueryWrapper<Permission>().lambda()
                .like(ObjectUtil.isNotEmpty(bo.getPermissionCode()), Permission::getPermissionCode, bo.getPermissionCode())
                .like(ObjectUtil.isNotEmpty(bo.getPermissionName()), Permission::getPermissionName, bo.getPermissionName())
                .eq(ObjectUtil.isNotEmpty(bo.getStatus()), Permission::getStatus, bo.getStatus()));

        Page<PermissionListVo> permissionListVoPage = new Page<>();
        permissionListVoPage.setRecords(BeanUtil.copyToList(permissionPage.getRecords(),PermissionListVo.class));
        permissionListVoPage.setSize(permissionPage.getSize());
        permissionListVoPage.setTotal(permissionPage.getTotal());
        return permissionListVoPage;
    }

    @Override
    public void updatePermission(PermissionUpdateBo bo) throws Exception {
        Permission permission = BeanUtil.copyProperties(bo, Permission.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        permission.setUpdateUserId(authentication.getId());
        Permission permissionEn = permissionMapper.selectById(permission.getId());
        permission.setVersion(permissionEn.getVersion());
        int i = permissionMapper.updateById(permission);
        if (ConstantCode.INT_ONE != i){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }

    @Override
    public void savePermission(PermissionSaveBo bo) throws Exception {
        Permission permission = BeanUtil.copyProperties(bo, Permission.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        permission
                .setCreateUserId(authentication.getId())
                .setUpdateUserId(authentication.getId())
                .setStatus(ConstantCode.STR_Z_ONE);
        int i = permissionMapper.insert(permission);
        if (ConstantCode.INT_ONE != i){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delPermission(PermissionDelBo bo) throws Exception {
        List<String> idList = bo.getIdList();
        int deleteBatchIds = permissionMapper.deleteBatchIds(idList);
        if (idList.size() != deleteBatchIds){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }
}
