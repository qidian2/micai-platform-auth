package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2022/1/14 9:26
 * @ClassName UserRoleUpdateBo
 * @Description
 */
@Data
@ApiModel(value = "UserRoleUpdateBo")
public class UserRoleUpdateBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id")
    private Long id;

    @NotNull(message = "用户id不能为空")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    @NotNull(message = "角色id不能为空")
    @ApiModelProperty(value = "角色id")
    private Long roleId;

}
