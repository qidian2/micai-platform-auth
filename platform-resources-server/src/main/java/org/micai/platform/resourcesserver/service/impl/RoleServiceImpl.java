package org.micai.platform.resourcesserver.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.resourcesserver.bo.RoleDelBo;
import org.micai.platform.resourcesserver.bo.RoleFindBo;
import org.micai.platform.resourcesserver.bo.RoleSaveBo;
import org.micai.platform.resourcesserver.bo.RoleUpdateBo;
import org.micai.platform.resourcesserver.dto.UserAuthenticationDto;
import org.micai.platform.resourcesserver.entity.Role;
import org.micai.platform.resourcesserver.mapper.RoleMapper;
import org.micai.platform.resourcesserver.service.RoleService;
import org.micai.platform.resourcesserver.utils.AuthenticationManger;
import org.micai.platform.resourcesserver.vo.RoleListVo;
import org.micai.platform.resourcesserver.vo.UserListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Page<RoleListVo> getRoleList(RoleFindBo bo) throws Exception {
        Page<Role> page = new Page<>(bo.getPageNumber(), bo.getPageSize());
        Page<Role> rolePage = roleMapper.selectPage(page, new QueryWrapper<Role>().lambda()
                .like(ObjectUtil.isNotEmpty(bo.getRoleCode()), Role::getRoleCode, bo.getRoleCode())
                .like(ObjectUtil.isNotEmpty(bo.getRoleName()), Role::getRoleName, bo.getRoleName())
                .eq(ObjectUtil.isNotEmpty(bo.getStatus()), Role::getStatus, bo.getStatus()));

        Page<RoleListVo> roleListVoPage = new Page<>();
        roleListVoPage.setRecords(BeanUtil.copyToList(rolePage.getRecords(),RoleListVo.class));
        roleListVoPage.setSize(rolePage.getSize());
        roleListVoPage.setTotal(rolePage.getTotal());
        return roleListVoPage;
    }

    @Override
    public void updateRole(RoleUpdateBo bo) throws Exception {
        Role role = BeanUtil.copyProperties(bo, Role.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        role.setUpdateUserId(authentication.getId());
        Role roleEn = roleMapper.selectById(role.getId());
        role.setVersion(roleEn.getVersion());
        int i = roleMapper.updateById(role);
        if (ConstantCode.INT_ONE != i){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }

    @Override
    public void saveRole(RoleSaveBo bo) throws Exception {
        Role role = BeanUtil.copyProperties(bo, Role.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        role.setRoleCode(RandomUtil.randomString(ConstantCode.INT_TEN))
                .setCreateUserId(authentication.getId())
                .setUpdateUserId(authentication.getId())
                .setStatus(ConstantCode.STR_Z_ONE);
        int i = roleMapper.insert(role);
        if (ConstantCode.INT_ONE != i){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delRole(RoleDelBo bo) throws Exception {
        List<String> idList = bo.getIdList();
        int deleteBatchIds = roleMapper.deleteBatchIds(idList);
        if (idList.size() != deleteBatchIds){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }
}
