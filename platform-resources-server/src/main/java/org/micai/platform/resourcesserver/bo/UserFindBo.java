package org.micai.platform.resourcesserver.bo;

import cn.hutool.db.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/8 15:01
 * @ClassName UserFindBo
 * @Description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "用户查询")
public class UserFindBo  extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;
}
