package org.micai.platform.resourcesserver.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.resourcesserver.bo.UserRoleDelBo;
import org.micai.platform.resourcesserver.bo.UserRoleSaveBo;
import org.micai.platform.resourcesserver.bo.UserRoleUpdateBo;
import org.micai.platform.resourcesserver.dto.UserAuthenticationDto;
import org.micai.platform.resourcesserver.entity.UserRole;
import org.micai.platform.resourcesserver.mapper.UserRoleMapper;
import org.micai.platform.resourcesserver.service.UserRoleService;
import org.micai.platform.resourcesserver.utils.AuthenticationManger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public void updateUR(UserRoleUpdateBo bo) throws Exception {
        UserRole userRole = BeanUtil.copyProperties(bo, UserRole.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        userRole.setUpdateUserId(authentication.getId());
        UserRole userRoleEn = userRoleMapper.selectById(userRole.getId());
        userRole.setVersion(userRoleEn.getVersion());
        userRoleMapper.updateById(userRole);
    }

    @Override
    public void saveUR(UserRoleSaveBo bo) throws Exception {
        UserRole userRole = BeanUtil.copyProperties(bo, UserRole.class);

        //不能有重复的
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda()
                .eq(UserRole::getUserId, bo.getUserId())
                .eq(UserRole::getRoleId, bo.getRoleId()));

        if (ObjectUtil.isNotEmpty(userRoles)) {
            throw new PlatformException(ConstantEnum.DATA_DUPLICATION);
        }

        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        userRole.setCreatUserId(authentication.getId())
                .setUpdateUserId(authentication.getId());
        userRoleMapper.insert(userRole);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delUR(UserRoleDelBo bo) throws Exception {
        List<String> idList = bo.getIdList();
        int deleteBatchIds = userRoleMapper.deleteBatchIds(idList);
        if (idList.size() != deleteBatchIds){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }
}
