package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @Author liuCong
 * @Date 2021/12/11 21:00
 * @ClassName UserDelBo
 * @Description
 */
@Data
@ApiModel(value = "用户删除")
public class UserDelBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "idList", required = true)
    @NotEmpty(message = "id不能为空")
    private List<String> idList;

}
