package org.micai.platform.resourcesserver.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.micai.platform.resourcesserver.bo.UserDelBo;
import org.micai.platform.resourcesserver.bo.UserFindBo;
import org.micai.platform.resourcesserver.bo.UserSaveBo;
import org.micai.platform.resourcesserver.bo.UserUpdateBo;
import org.micai.platform.resourcesserver.entity.User;
import org.micai.platform.resourcesserver.vo.UserListVo;

import java.util.List;

public interface UserService extends IService<User> {

    User findByUsername(String username);

    List<User> findUserList();

    Page<UserListVo> getUserList(UserFindBo bo) throws Exception;

    void updateUser(UserUpdateBo bo) throws Exception;

    String saveUser(UserSaveBo bo)throws Exception;

    void delUser(UserDelBo bo) throws Exception;
}
