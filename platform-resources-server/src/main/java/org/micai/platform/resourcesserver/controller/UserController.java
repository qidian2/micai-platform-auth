package org.micai.platform.resourcesserver.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.common.base.result.Result;
import org.micai.platform.resourcesserver.bo.UserDelBo;
import org.micai.platform.resourcesserver.bo.UserFindBo;
import org.micai.platform.resourcesserver.bo.UserSaveBo;
import org.micai.platform.resourcesserver.bo.UserUpdateBo;
import org.micai.platform.resourcesserver.entity.User;
import org.micai.platform.resourcesserver.vo.UserListVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * @author zhaoxinguo on 2017/9/13.
 */
@Slf4j
@Api(tags = "用户管理", value = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    /**
     * 注册用户 默认开启白名单
     * @param user
     */
    @ApiIgnore
    @ApiOperation(value = "注册用户",notes = "注册用户")
    @PostMapping("/signup")
    public Result signup(@RequestBody User user) {
        User bizUser = userService.findByUsername(user.getUsername());
        if(null != bizUser){
            throw new PlatformException(ConstantEnum.USER_EXIST);
        }
        /*user.setPassword(DigestUtils.md5DigestAsHex((user.getPassword()).getBytes()));*/
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        boolean flag = userService.save(user);
        return new Result(ConstantEnum.SUCCESS,flag);
    }

    /**
     * 获取用户权限
     * @return
     */
    @ApiIgnore
    @ApiOperation(value = "获取用户权限",notes = "获取用户权限")
    @GetMapping("/authorityList")
    @PreAuthorize("hasAnyAuthority('sys:user:authorityList')")
    public List<String> authorityList(){
        return getAuthentication();
    }


    /**
     * 获取用户列表
     * @return
     */
    @ApiOperation(value = "获取用户列表", notes = "获取用户列表")
    @PostMapping("/list")
    @PreAuthorize("hasAnyAuthority('sys:user:list')")
    public Result getUserList(@RequestBody UserFindBo bo){
        Result result;
        try {
            Page<UserListVo> page = userService.getUserList(bo);
            result = new Result(ConstantEnum.SUCCESS,page);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }


    @ApiOperation(value = "更新用户", notes = "更新用户")
    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('sys:user:update')")
    public Result updateUser(@RequestBody @Valid UserUpdateBo bo){
        Result result;
        try {
            userService.updateUser(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "新增用户", notes = "新增用户")
    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('sys:user:save')")
    public Result saveUser(@RequestBody @Valid UserSaveBo bo){
        Result result;
        try {
            String password = userService.saveUser(bo);
            result = new Result(ConstantEnum.SUCCESS,password);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "删除用户", notes = "删除用户")
    @DeleteMapping("/del")
    @PreAuthorize("hasAnyAuthority('sys:user:del')")
    public Result delUser(@RequestBody @Valid UserDelBo bo){
        Result result;
        try {
            userService.delUser(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }
}
