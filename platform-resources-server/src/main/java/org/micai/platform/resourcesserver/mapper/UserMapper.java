package org.micai.platform.resourcesserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.micai.platform.resourcesserver.entity.User;

public interface UserMapper extends BaseMapper<User> {

    User findByUsername(String username);
}
