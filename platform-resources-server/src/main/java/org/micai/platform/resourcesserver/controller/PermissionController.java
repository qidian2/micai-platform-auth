package org.micai.platform.resourcesserver.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.common.base.result.Result;
import org.micai.platform.resourcesserver.bo.PermissionDelBo;
import org.micai.platform.resourcesserver.bo.PermissionFindBo;
import org.micai.platform.resourcesserver.bo.PermissionSaveBo;
import org.micai.platform.resourcesserver.bo.PermissionUpdateBo;
import org.micai.platform.resourcesserver.service.PermissionService;
import org.micai.platform.resourcesserver.vo.PermissionListVo;
import org.micai.platform.resourcesserver.vo.RoleListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@RestController
@RequestMapping("/permission")
@Api(tags = "权限管理",value = "权限管理")
@Slf4j
public class PermissionController extends BaseController{

    @Autowired
    private PermissionService permissionService;

    @ApiOperation(value = "获取权限列表", notes = "获取权限列表")
    @PostMapping("/list")
    @PreAuthorize("hasAnyAuthority('sys:permission:list')")
    public Result getPermissionList(@RequestBody PermissionFindBo bo){
        Result result;
        try {
            Page<PermissionListVo> page = permissionService.getPermissionList(bo);
            result = new Result(ConstantEnum.SUCCESS,page);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }


    @ApiOperation(value = "更新权限信息", notes = "更新权限信息")
    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('sys:permission:update')")
    public Result updatePermission(@RequestBody @Valid PermissionUpdateBo bo){
        Result result;
        try {
            permissionService.updatePermission(bo);
            result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "新增权限", notes = "新增权限")
    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('sys:permission:save')")
    public Result savePermission(@RequestBody @Valid PermissionSaveBo bo){
        Result result;
        try {
            permissionService.savePermission(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "删除权限", notes = "删除权限")
    @DeleteMapping("/del")
    @PreAuthorize("hasAnyAuthority('sys:permission:del')")
    public Result delPermission(@RequestBody @Valid PermissionDelBo bo){
        Result result;
        try {
            permissionService.delPermission(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

}

