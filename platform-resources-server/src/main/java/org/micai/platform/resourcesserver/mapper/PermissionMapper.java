package org.micai.platform.resourcesserver.mapper;

import org.micai.platform.resourcesserver.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
