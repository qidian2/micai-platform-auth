package org.micai.platform.resourcesserver.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.resourcesserver.bo.RolePermissionDelBo;
import org.micai.platform.resourcesserver.bo.RolePermissionSaveBo;
import org.micai.platform.resourcesserver.bo.RolePermissionUpdateBo;
import org.micai.platform.resourcesserver.dto.UserAuthenticationDto;
import org.micai.platform.resourcesserver.entity.RolePermission;
import org.micai.platform.resourcesserver.mapper.RolePermissionMapper;
import org.micai.platform.resourcesserver.service.RolePermissionService;
import org.micai.platform.resourcesserver.utils.AuthenticationManger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {
    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Override
    public void updateRP(RolePermissionUpdateBo bo) throws Exception {
        RolePermission rolePermission = BeanUtil.copyProperties(bo, RolePermission.class);
        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        rolePermission.setUpdateUserId(authentication.getId());
        RolePermission rolePermissionEn = rolePermissionMapper.selectById(rolePermission.getId());
        rolePermission.setVersion(rolePermissionEn.getVersion());
        rolePermissionMapper.updateById(rolePermission);
    }

    @Override
    public void saveRP(RolePermissionSaveBo bo) throws Exception {
        RolePermission rolePermission = BeanUtil.copyProperties(bo, RolePermission.class);

        //不能有重复的
        List<RolePermission> rolePermissions = rolePermissionMapper.selectList(new QueryWrapper<RolePermission>().lambda()
                .eq(RolePermission::getPermissionId, bo.getPermissionId())
                .eq(RolePermission::getRoleId, bo.getRoleId()));

        if (ObjectUtil.isNotEmpty(rolePermissions)) {
            throw new PlatformException(ConstantEnum.DATA_DUPLICATION);
        }

        UserAuthenticationDto authentication = AuthenticationManger.getAuthentication();
        rolePermission
                .setCreateUserId(authentication.getId())
                .setUpdateUserId(authentication.getId());
        rolePermissionMapper.insert(rolePermission);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delRP(RolePermissionDelBo bo) throws Exception {
        List<String> idList = bo.getIdList();
        int deleteBatchIds = rolePermissionMapper.deleteBatchIds(idList);
        if (idList.size() != deleteBatchIds){
            throw new PlatformException(ConstantEnum.UPDATE_ERROR);
        }
    }
}
