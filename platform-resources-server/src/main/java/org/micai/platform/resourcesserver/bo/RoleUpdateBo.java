package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/29 15:49
 * @ClassName RoleUpdateBo
 * @Description
 */
@Data
@ApiModel(value = "角色更新bo")
public class RoleUpdateBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @NotBlank(message = "主键id 不能为空")
    private Long id;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;
}
