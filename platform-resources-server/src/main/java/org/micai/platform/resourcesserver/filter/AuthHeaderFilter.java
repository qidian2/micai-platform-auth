package org.micai.platform.resourcesserver.filter;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantCode;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

/**
 * @Author liuCong
 * @Date 2022/12/12 上午 9:58
 * @ClassName AuthHeaderFilter
 * @Description
 */
@Slf4j
public class AuthHeaderFilter implements Filter {

    //拦截接口让OAuth2AuthenticationProcessingFilter去验证
    private final List<AntPathRequestMatcher> authOAuth2RequestMatcher = new ArrayList<>();

    public void setAuthHeaderRequestMatcher(List<String> oauth2AuthList) {
        Assert.notNull(authOAuth2RequestMatcher, "authOAuth2RequestMatcher cannot be null");

        if (ObjectUtil.isNotEmpty(oauth2AuthList)) {
            for (String oauth2Auth : oauth2AuthList) {
                authOAuth2RequestMatcher.add(new AntPathRequestMatcher(oauth2Auth));
            }
        }
        log.info("init request matcher list:==========================>>>>>>>>>>>>>>>>> \n" +
                        "authOAuth2RequestMatcher{} ", authOAuth2RequestMatcher);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletRequestWrapper requestWrapper = new HttpServletRequestWrapper((HttpServletRequest) servletRequest) {


            /**
             * 得到头
             *为了让在
             *JWTAuthenticationFilter 获取token时为空
             * @param name 名字
             * @return {@link String}
             */
            @Override
            public String getHeader(String name) {
                if (ObjectUtil.isEmpty(authOAuth2RequestMatcher)){
                    return super.getHeader(name);
                }
                boolean isMatcher = Boolean.TRUE;
                if (ObjectUtil.isNotEmpty(authOAuth2RequestMatcher)) {
                    for (AntPathRequestMatcher antPathRequestMatcher : authOAuth2RequestMatcher) {
                        if (antPathRequestMatcher.matches(request) && ConstantCode.AUTHORIZATION.equalsIgnoreCase(name)) {
                            isMatcher = Boolean.FALSE;
                            break;
                        }
                    }
                }
                if (isMatcher){
                    return super.getHeader(name);
                }else {
                    return null;
                }
            }

            /**
             * 重写得到头方法
             * 为了让在
             * OAuth2AuthenticationProcessingFilter
             * org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor#extractHeaderToken(javax.servlet.http.HttpServletRequest)
             * 获取的token时候为空
             * @param name 名字
             * @return {@link Enumeration}<{@link String}>
             */
            @Override
            public Enumeration<String> getHeaders(String name) {
                if (ObjectUtil.isNotEmpty(authOAuth2RequestMatcher)) {
                    for (AntPathRequestMatcher antPathRequestMatcher : authOAuth2RequestMatcher) {
                        if (antPathRequestMatcher.matches(request) && ConstantCode.AUTHORIZATION.equalsIgnoreCase(name)) {
                            return super.getHeaders(name);
                        }
                    }
                }
                return new Vector<String>().elements();
            }
        };
        filterChain.doFilter(requestWrapper, servletResponse);
    }
}
