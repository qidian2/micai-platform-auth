package org.micai.platform.resourcesserver.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.exception.PlatformException;
import org.micai.platform.common.base.result.Result;
import org.micai.platform.resourcesserver.bo.RoleDelBo;
import org.micai.platform.resourcesserver.bo.RoleFindBo;
import org.micai.platform.resourcesserver.bo.RoleSaveBo;
import org.micai.platform.resourcesserver.bo.RoleUpdateBo;
import org.micai.platform.resourcesserver.service.RoleService;
import org.micai.platform.resourcesserver.vo.RoleListVo;
import org.micai.platform.resourcesserver.vo.UserListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
@RestController
@RequestMapping("/role")
@Api(tags = "角色管理", value = "角色管理")
@Slf4j
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "获取角色列表", notes = "获取角色列表")
    @PostMapping("/list")
    @PreAuthorize("hasAnyAuthority('sys:role:list')")
    public Result getRoleList(@RequestBody RoleFindBo bo){
        Result result;
        try {
            Page<RoleListVo> page = roleService.getRoleList(bo);
            result = new Result(ConstantEnum.SUCCESS,page);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }


    @ApiOperation(value = "更新角色信息", notes = "更新角色信息")
    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('sys:role:update')")
    public Result updateRole(@RequestBody @Valid RoleUpdateBo bo){
        Result result;
        try {
            roleService.updateRole(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "新增角色", notes = "新增角色")
    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('sys:role:save')")
    public Result saveRole(@RequestBody @Valid RoleSaveBo bo){
        Result result;
        try {
            roleService.saveRole(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

    @ApiOperation(value = "删除角色", notes = "删除角色")
    @DeleteMapping("/del")
    @PreAuthorize("hasAnyAuthority('sys:role:del')")
    public Result delRole(@RequestBody @Valid RoleDelBo bo){
        Result result;
        try {
            roleService.delRole(bo);
             result = new Result(ConstantEnum.SUCCESS);
        } catch (PlatformException e) {
            log.error("error info:",e);
            result =  new Result(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            log.error("error info:",e);
            result =  new Result(ConstantEnum.FAIL);
        }
        return result;
    }

}

