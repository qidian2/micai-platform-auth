package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2022/1/13 16:57
 * @ClassName PermissionMenuUpdateBo
 * @Description
 */
@Data
@ApiModel(value = "PermissionMenuUpdateBo")
public class PermissionMenuUpdateBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id")
    private Long id;

    @NotNull(message = "权限id不能为空")
    @ApiModelProperty(value = "权限id")
    private Long permissionId;

    @NotNull(message = "菜单id不能为空")
    @ApiModelProperty(value = "菜单id")
    private Long menuId;
}
