package org.micai.platform.resourcesserver.utils;

import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantCode;
import org.micai.platform.resourcesserver.dto.UserAuthenticationDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author liuCong
 * @Date 2021/12/7 10:29
 * @ClassName AuthenticationManger
 * @Description
 */
@Slf4j
public class AuthenticationManger {

    /**
     * 获取用户所拥有的权限列表
     *
     * @return
     */
    public static UserAuthenticationDto getAuthentication() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<String> permissionList = new ArrayList<>();
        List<String> roleList = new ArrayList<>();
        for (GrantedAuthority grantedAuthority : authorities) {
            String authority = grantedAuthority.getAuthority();
            if (authority.contains(ConstantCode.DEFAULT_ROLE_PREFIX)) {
                roleList.add(authority);
                continue;
            }
            permissionList.add(authority);
        }
        UserAuthenticationDto userAuthenticationDto = new UserAuthenticationDto();
        String userId = authentication.getName().split("-")[ConstantCode.INT_ZERO];
        userAuthenticationDto.setId(Long.parseLong(userId));
        userAuthenticationDto.setRoles(roleList);
        userAuthenticationDto.setPermissions(permissionList);
        log.info("用户id ：{}, 角色列表：{}, 权限列表：{}", userAuthenticationDto.getId(), roleList, permissionList);
        return userAuthenticationDto;
    }
}
