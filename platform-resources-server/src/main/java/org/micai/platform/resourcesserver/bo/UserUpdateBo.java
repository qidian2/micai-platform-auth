package org.micai.platform.resourcesserver.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/9 19:36
 * @ClassName UserUpateBo
 * @Description
 */
@Data
@ApiModel(value = "用户更新")
public class UserUpdateBo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id", required = true)
    @NotBlank(message = "主键id不能为空")
    private String id;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "email")
    private String email;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;
}
