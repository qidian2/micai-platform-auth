package org.micai.platform.resourcesserver.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.micai.platform.resourcesserver.bo.PermissionDelBo;
import org.micai.platform.resourcesserver.bo.PermissionFindBo;
import org.micai.platform.resourcesserver.bo.PermissionSaveBo;
import org.micai.platform.resourcesserver.bo.PermissionUpdateBo;
import org.micai.platform.resourcesserver.entity.Permission;
import org.micai.platform.resourcesserver.vo.PermissionListVo;
import org.micai.platform.resourcesserver.vo.RoleListVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface PermissionService extends IService<Permission> {

    Page<PermissionListVo> getPermissionList(PermissionFindBo bo) throws Exception;

    void updatePermission(PermissionUpdateBo bo) throws Exception;

    void savePermission(PermissionSaveBo bo) throws Exception;

    void delPermission(PermissionDelBo bo) throws Exception;
}
