package org.micai.platform.resourcesserver.mapper;

import org.micai.platform.resourcesserver.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
