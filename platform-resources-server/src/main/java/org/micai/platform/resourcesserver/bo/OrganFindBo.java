package org.micai.platform.resourcesserver.bo;

import cn.hutool.db.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author liuCong
 * @Date 2021/12/29 15:35
 * @ClassName OrganFindBo
 * @Description
 */
@Data
@ApiModel(value = "查询组织bo")
public class OrganFindBo extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组织编码")
    private String organCode;

    @ApiModelProperty(value = "组织名称")
    private String organName;

    @ApiModelProperty(value = "状态: 01 启用 02停用")
    private String status;

}
