package org.micai.platform.resourcesserver.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.micai.platform.resourcesserver.bo.RoleDelBo;
import org.micai.platform.resourcesserver.bo.RoleFindBo;
import org.micai.platform.resourcesserver.bo.RoleSaveBo;
import org.micai.platform.resourcesserver.bo.RoleUpdateBo;
import org.micai.platform.resourcesserver.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import org.micai.platform.resourcesserver.vo.RoleListVo;
import org.micai.platform.resourcesserver.vo.UserListVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface RoleService extends IService<Role> {

    Page<RoleListVo> getRoleList(RoleFindBo bo)throws Exception;

    void updateRole(RoleUpdateBo bo) throws Exception;

    void saveRole(RoleSaveBo bo) throws Exception;

    void delRole(RoleDelBo bo)throws Exception;
}
