### 1、初始化micai_platform.sql报错

报错信息如下：

Specified key was too long; max key length is 767 bytes

解决方案：

在数据库执行下面的语句后，再执行就好了

```
SET GLOBAL innodb_large_prefix = ON
SET GLOBAL innodb_file_format = BARRACUDA
```