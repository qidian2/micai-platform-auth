package org.micai.platform.common.base.exception;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.micai.platform.common.base.constant.ConstantEnum;
import org.micai.platform.common.base.result.Result;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.net.ConnectException;
import java.util.stream.Collectors;

/**
 * @ClassName GlobalExceptionHandler
 * @Description 全局处理自定义的业务异常
 * @Author zhaoxinguo
 * @Date 2021/11/26 15:59
 * @Version 1.0
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    public Result handlerNoFoundException(Exception e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.NOT_FOUND);
    }

    @ExceptionHandler(ConnectException.class)
    public Result connectException(ConnectException e){
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.REQUEST_TIMEOUT);
    }

    @ExceptionHandler(ResourceAccessException.class)
    public Result connectException(ResourceAccessException e){
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.RESOURCE_ACCESS);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public Result accessDeniedException(AccessDeniedException e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.ACCESS_DENIED);
    }

    @ExceptionHandler(SignatureException.class)
    public Result signatureException(SignatureException e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.SIGNATURE_EX);
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public Result expiredJwtException(ExpiredJwtException e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.EXPIRED_JWT);
    }

    @ExceptionHandler(UnsupportedJwtException.class)
    public Result unsupportedJwtException(UnsupportedJwtException e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.TOKEN_ERROR);
    }

    @ExceptionHandler(MalformedJwtException.class)
    public Result MalformedJwtException(MalformedJwtException e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.ACCESS_DENIED);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public Result illegalArgumentException(IllegalArgumentException e) {
        log.error(e.getMessage(), e);
        return new Result(ConstantEnum.ILLEGAL_ARG);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result methodArgumentNotValidException(MethodArgumentNotValidException exception) {
        log.info("捕获异常MethodArgumentNotValidException");
        String message = exception.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining());
        return new Result(ConstantEnum.FAIL,message);
    }
}
