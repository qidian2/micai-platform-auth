package org.micai.platform.common.base.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author liuCong
 * @Date 2022/12/13 上午 9:38
 * @ClassName MicaiPlatformAuthConfig
 * @Description
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "micai-platform-auth.oauth-auth")
public class MicaiPlatformOauthConfig {

    /** JWT*/
    private String signKey;

    /** access_token的长久有效期(单位:分钟) 默认1分钟*/
    private Integer tokenTimeout = 1;

    /** refresh_token的长久有效期(单位:分钟) 默认1分钟*/
    private Integer refreshTimeout = 1;

    public Integer getTokenTimeout() {
        return tokenTimeout;
    }

    public Integer getRefreshTimeout() {
        return refreshTimeout;
    }

}
