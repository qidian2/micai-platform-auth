package org.micai.platform.common.base.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @Author liuCong
 * @Date 2022/12/13 上午 9:38
 * @ClassName MicaiPlatformAuthConfig
 * @Description
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "micai-platform-auth.oauth-resources")
public class MicaiPlatformResourcesConfig {

    /** JWT*/
    private String signKey;
    private String resourceIds;
    private List<String> requestMatcher;

}
