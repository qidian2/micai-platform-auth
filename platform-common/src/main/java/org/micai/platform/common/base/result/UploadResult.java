package org.micai.platform.common.base.result;

import lombok.Data;
import lombok.experimental.Accessors;
import org.micai.platform.common.base.constant.ConstantEnum;

/**
 * @Author liuCong
 * @Date 2021/12/30 10:04
 * @ClassName UploadResult
 * @Description
 */
@Data
@Accessors(chain = true)
public class UploadResult{

    /**
     * 上传状态
     */
    private Integer status;

    /**
     * 提示文字
     */
    private String message;

    /**
     * 文件名
     */
    private String name;

    /**
     * 文件大小
     */
    private long size;

    /**
     * 文件存放路径
     */
    private String path;

    public UploadResult(ConstantEnum constantEnum) {
        this.status = constantEnum.getCode();
        this.message = constantEnum.getMessage();
    }
}
