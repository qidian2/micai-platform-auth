package org.micai.platform.common.base;

import org.micai.platform.common.base.controller.ExceptionController;
import org.micai.platform.common.base.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Author liuCong
 * @Date 2022/12/12 下午 3:55
 * @ClassName WebStarterAutoConfig
 * @Description
 */
@Configuration
@Import({GlobalExceptionHandler.class, ExceptionController.class})
public class WebStarterAutoConfig {

    public WebStarterAutoConfig() {
    }
}
