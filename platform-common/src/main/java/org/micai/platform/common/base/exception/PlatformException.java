package org.micai.platform.common.base.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.micai.platform.common.base.constant.ConstantEnum;

/**
 * @Author liuCong
 * @Date 2021/12/7 11:16
 * @ClassName PlatformException
 * @Description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PlatformException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    private Integer errorCode;
    private String errorMessage;

    public PlatformException() {
        super();
    }

    public PlatformException(Throwable cause) {
        super(cause);
    }

    public PlatformException(String msg, Throwable cause) {
        super(msg, cause);
        this.errorMessage = msg;
    }

    public PlatformException(Integer code, String msg) {
        super(msg);
        this.errorCode = code;
        this.errorMessage = msg;
    }

    public PlatformException(Integer errorCode, String errorMessage, Throwable cause) {
        super("[" + errorCode + "]" + errorMessage, cause);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public PlatformException(ConstantEnum constantEnum) {
        this(constantEnum.getCode(), constantEnum.getMessage());
    }
}
