package org.micai.platform.authserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.micai.platform.authserver.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liucong
 * @since 2021-12-04
 */
public interface UserRoleService extends IService<UserRole> {


}
