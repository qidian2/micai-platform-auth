package org.micai.platform.authserver.config;

import org.micai.platform.common.base.config.MicaiPlatformOauthConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @Author liuCong
 * @Date 2022/11/24 下午 3:38
 * @ClassName JwtTokenConfig
 * @Description 配置Jwt令牌服务，生成jwt格式的token
 */
@Configuration
public class TokenConfig {

    @Autowired
    private MicaiPlatformOauthConfig micaiPlatformOauthConfig;

    /**
     * @Author : liuCong
     * @Date : 2022/11/25 下午 5:02
     * @Description :
     * @Throws : //
     * @Params : []
     * @Return : org.springframework.security.oauth2.provider.token.store.JwtTokenStore
     * 默认使用的是InMemoryTokenStore来存储，如果用数据库，那么每次token服务查询、存储，都需要SQL操作。
     **/
    @Bean
    public JwtTokenStore jwtTokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    /**
     * @Author : liuCong
     * @Date : 2022/11/25 下午 4:16
     * @Description :TokenEnhancer的子类，帮助程序在JWT编码的令牌值和OAuth身份验证信息之间进行转换（在两个方向上），同时充当TokenEnhancer授予令牌的时间。
     * @Throws : //
     * @Params : []
     * @Return : org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
     **/
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(micaiPlatformOauthConfig.getSignKey());
        return converter;
    }
}
