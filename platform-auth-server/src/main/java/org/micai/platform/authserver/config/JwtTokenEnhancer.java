package org.micai.platform.authserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author liuCong
 * @Date 2022/11/24 下午 2:40
 * @ClassName JwtTokenEnhancerConfig
 * @Description
 */
@Configuration
public class JwtTokenEnhancer implements TokenEnhancer {

    /**
     * @Author : liuCong
     * @Date : 2022/11/25 下午 4:36
     * @Description :
     * org.springframework.security.oauth2.provider.token.DefaultTokenServices 默认token创建方法
     * 在 AuthorizationServerTokenServices 增强访问令牌的策略。
     * org.springframework.security.oauth2.provider.token.TokenEnhancer#enhance(org.springframework.security.oauth2.common.OAuth2AccessToken, org.springframework.security.oauth2.provider.OAuth2Authentication)
     * @Throws : //
     * @Params : [oAuth2AccessToken, oAuth2Authentication]
     * @Return : org.springframework.security.oauth2.common.OAuth2AccessToken
     **/
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        Map<String, Object> info = new HashMap<>();
        //自定义的信息
        info.put("author", "liucong");
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(info);
        return oAuth2AccessToken;
    }


}
