package org.micai.platform.authserver.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.micai.platform.authserver.entity.User;
import org.micai.platform.authserver.mapper.UserMapper;
import org.micai.platform.authserver.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @ClassName UserServiceImpl
 * @Description 用户Service
 * @Author zhaoxinguo
 * @Date 2021/12/3 20:40
 * @Version 1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
